#include <stdio.h>

int solution(int a[], int N){
   int i, j, result = 1;
   for (i = 1; i < N; i++){
      for (j = 0; j < i; j++){
         if (a[i] == a[j]){
            break;
         }
      }
      if (i == j){
         result++;
      }
   }
   return result;
}

int main(){
   int a[] = { -4, 7, 10, 9, 12, 6, 7, 8 };
   int size = sizeof(a) / sizeof(a[0]);
   printf("Result is %d\n", solution(a, size));
   return 0;
}
