#ifndef HAL_H
#define HAL_H

#define GPIOA         0x1000
#define ADC_REG       0x2000
#define TIMER         0x3000

#define TRUE          1
#define FALSE         0

#endif
