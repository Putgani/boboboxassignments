#include <stdint.h> // for uintXX_t
#include "HAL.h"
#include "pid.h"

// Function prototypes
void HAL_Init(void);
void HeartBeat(void);
float Read_Temperature(void);
// Bang-bang a.k.a on/off controller
void Bangbang_Controller(int16_t measurement, int16_t upperBound, int16_t lowerBound);
void Error_Handler(void);

// pointers for peripherals' addreses
volatile uint16_t *PORTA = 0x0;
volatile uint16_t *ADC = 0x0;

uint16_t count_millisecond_led = 0; // counter for heartbeat
uint16_t count_millisecond_adc = 0; // counter for sampling period
uint16_t adc_sampling_period = 200; // Sampling period in millisecond for the controller
uint16_t temperature = 0; // placeholder for adc value
uint8_t is_adc_ready = FALSE; // Set flag to TRUE when sampling period has elapsed

//Bangbang Controller variables
uint8_t upperBound_Bangbang = 18; // in Celcius.
uint8_t lowerBound_Bangbang = 14;

int main()
{
    // Initialize the registers.
    HAL_Init();

    while(1)
    {
        /* A temperature controller system keeps 
         * a room temperature few degrees Celcius 
         * below standard room temperature (20*C).
         */

        while(is_adc_ready)
        {
            Bangbang_Controller(temperature, upperBound_Bangbang, lowerBound_Bangbang);
            is_adc_ready = FALSE;

            /* If we have advanced timer with Output Compare or PWM mode, 
             * then we can control the PWM signal comming out of PORTA pin 1, 
             * assumming PORTA pin 1 can be programmed as Timer's channel.
             * If we can do that then we can use more sophisticated controller such as PID Controller
             * I've implemented the driver for PID controller.
             */

            // PID_ControllerInit(PID_ControllerTypeDef *pid);
            // PID_ControllerUpdate(PID_ControllerTypeDef *pid, float setpoint, float measurement);
        }
    }

    return 0;
}

void HAL_Init()
{
    PORTA = (uint16*)GPIOA;
    *PORTA = 0x0;

    ADC = (uint16*)ADC_REG;
}

void TIMER_ISR()  // Timer interrupts every 1ms
{
    /*
     * I assume this Timer's ISR is already defined in startup file with weak attribute.
     * The assignment says this 32-bit Timer is configured as 1ms per tick, decrementing.
     * So I assume the System Clock and Prescalar have been configured properly, since their addresses are not provided. 
     */
    ++count_millisecond_LED;
    ++count_millisecond_ADC;

    void HeartBeat();

    if(!is_adc_ready) // Protect shared variable, best practice
    {
        // Read the sensor value every sampling period
        if(count_millisecond_adc = adc_sampling_period)
        {
            count_millisecond_adc = 0;
            temperature = Read_Temperature();  
            is_adc_ready = TRUE;
        }
    }
}

void HeartBeat()
{
    /*
     * Normal resting heartrate is around 1hz. 
     * So after 1 second elapsed, turn on the LED for 200 ms.
     */
    if(count_millisecond > 1200) 
    {
        *PORTA &= ~(1 << 0);
        count_millisecond = 0;
    }
    else if(count_millisecond > 1000)
    {
        *PORTA |= (1 << 0);
    }
}

inline float Read_Temperature()
{
    // Sensor temperature with sensitivity 1 deg C / 100 mV connected to 10 bit ADC
    // I assume data is formatted as two's complement.
    // To convert to Celcius, multiply the adc data with 0.1.
    return (  0.1 * ((int16_t)*ADC) ); 
}

void Bangbang_Controller(int16_t measurement, int16_t upperBound, int16_t lowerBound)
{
    if(measurement > upperBound)
    {
        *PORTA |= (1 << 1);
    }
    else if (measurement < lowerBound)
    {
        *PORTA &= ~(1 << 1);
    }
}

void Error_Handler(void)
{
        *PORTA &= ~(1 << 0);
        for(uint16_t i = 0; i < 50000; ++i);
        *PORTA |= (1 << 0);
        for(uint16_t i = 0; i < 50000; ++i);
}
