#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

typedef struct {

	/* Controller gains */
	float kp;
	float ki;
	float kd;

	/* Derivative low-pass filter time constant */
	float tau;

	/* Output limits */
	float limitMinimumOutput;
	float limitMaximumOutput;
	
	/* Integrator limits */
	float limitMinimumIntegrator;
	float limitMaximumIntegrator;

	/* Sample time (in seconds) */
	float deltaT;

	float integrator;
	float prevError;
	float differentiator;
	float prevMeasurement;

	/* Controller output */
	float out;

} PID_ControllerTypeDef;

void  PID_ControllerInit(PID_ControllerTypeDef *pid);
float PID_ControllerUpdate(PID_ControllerTypeDef *pid, float setpoint, float measurement);

#endif
